package com.voitech.lib.ejb.scheduler;

import com.voitech.lib.ejb.scheduler.converter.Converter;
import com.voitech.lib.ejb.scheduler.converter.ScheduledTask;
import java.io.IOException;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerService;
import javax.inject.Inject;

/**
 *
 * @author wkoszycki
 */
@Startup
@Singleton
public class Scheduler {

    private static final Logger LOG = Logger.getLogger(Scheduler.class.getName());
    @Resource
    private TimerService timerService;
    @Inject
    private Converter converter;
    private final Map<String, ScheduledTask> timers = new HashMap<String, ScheduledTask>();

    @PostConstruct
    private void init() {
        LOG.log(Level.INFO, "Initializing Scheduler");
        try {
            Map<String, ScheduledTask> deserialize = converter.deserialize();

            for (Map.Entry<String, ScheduledTask> entry : deserialize.entrySet()) {
                addTimer(entry.getValue());
            }

        } catch (IOException ex) {
            LOG.log(Level.SEVERE, null, ex);
        } catch (NoSuchMethodException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
    }

    @Timeout
    private void timeout(Timer timer) {

        ScheduledTask scheduled = timers.get(timer.getInfo().toString());
        if (!scheduled.isPaused()) {
            LOG.log(Level.INFO, "Scheduler Invoking timer {0}", timer.getInfo());
            scheduled.run();
        }
    }

    /**
     * Adding new timer to queue and overwriting existing one if same name
     * occurs
     *
     * @param tasks
     */
    public void addTimer(ScheduledTask... tasks) {
        for (ScheduledTask scheduled : tasks) {
            String timername = scheduled.getTimername();

            if (timers.containsKey(timername)) {
                removeTimer(timername);
            }
            timerService.createCalendarTimer(scheduled.getExpression(), scheduled.getTimerConfig());
            timers.put(timername, scheduled);
            LOG.log(Level.INFO, "Timer {0} added to queue, total timers {1}", new Object[]{timername, timers.size()});
        }
    }

    /**
     * Removing Timer from queue
     *
     * @param timer
     * @return true if timer was removed or false if not exist
     */
    public boolean removeTimer(String timer) {
        for (Timer tservice : timerService.getTimers()) {

            if (tservice.getInfo() != null && tservice.getInfo().toString().equals(timer)) {
                tservice.cancel();
                timers.remove(timer);
                return true;
            }
        }
        return false;
    }

    /**
     * Setting flag to not execute timer in queue
     *
     * @param timerNames name of timer
     */
    public void pause(String... timerNames) {
        for (String timer : timerNames) {
            ScheduledTask scheduled = timers.get(timer);
            if (scheduled != null) {
                scheduled.pause();
            }
        }
    }

    /**
     * Waking timer from pause state
     *
     * @param timerNames name of timer
     */
    public void wake(String... timerNames) {
        for (String timer : timerNames) {
            ScheduledTask scheduled = timers.get(timer);
            if (scheduled != null) {
                scheduled.play();
            }
        }
    }

    /**
     * Setting flag to pause all timers in queue
     */
    public void pauseAll() {

        for (String key : timers.keySet()) {
            timers.get(key).pause();
        }
    }

    /**
     * Waking all timers from pause state
     */
    public void wakeAll() {

        for (String key : timers.keySet()) {
            timers.get(key).play();
        }
    }

    /**
     * Deleting all timers from queue
     */
    public void clear() {
        for (Timer timer : timerService.getTimers()) {
            timer.cancel();
        }
        timers.clear();
    }

    public Map<String, ScheduledTask> getTimers() {
        return timers;
    }

    @PreDestroy
    private void onDestroy() {
        try {
            LOG.log(Level.SEVERE, "Serializing {0} timers", timers.size());
            converter.serialize(timers);
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
        }
    }
}
